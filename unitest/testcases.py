#!/usr/bin/env python
# -*- coding: utf-8 -*-

##
# -----------------------------------------------------------------------------
# (c) 2014 by Pascal Rapaz
# -----------------------------------------------------------------------------
#
# License: GPL-2+
#   This package is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This package is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   If you need the GNU General Public License write to:
#     Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
#     MA 02110-1301, USA.
#
# (c) 2014, Pascal Rapaz (RapazP - pascal.rapaz@rapazp.ch)
# =============================================================================
##

# __builtin__ is used to define inter-module variable "__dryrun__"
import __builtin__

import logging
import os
import unittest
import shutil

import usave

class TestCases(unittest.TestCase):

  TEST_BACKUP_RSYNC_FOLDER = "/tmp/backup_rsync"
  TEST_RESTORE_RSYNC_FOLDER = "/tmp/restore_rsync"

  TEST_BACKUP_TARFILE_FOLDER = "/tmp/backup_tarfile"
  TEST_RESTORE_TARFILE_FOLDER = "/tmp/restore_tarfile"

  @classmethod 
  def setUpClass(self):
    __builtin__.__dryrun__ = False

    self.log = usave._logger(usave.LOGGER_NAME, level=logging.NOTSET)

    test_folder = os.path.expanduser("~/Documents")
    test_file = os.path.expanduser("~/.bashrc")

    usave.PARTIAL_BACKUP = [test_folder, test_file]
  #endDef

  @classmethod 
  def tearDownClass(self):

    if (os.path.exists(self.TEST_BACKUP_RSYNC_FOLDER)):
      shutil.rmtree(self.TEST_BACKUP_RSYNC_FOLDER)
    #endIf

    if (os.path.exists(self.TEST_RESTORE_RSYNC_FOLDER)):
      shutil.rmtree(self.TEST_RESTORE_RSYNC_FOLDER)
    #endIf

    if (os.path.exists(self.TEST_BACKUP_TARFILE_FOLDER)):
      shutil.rmtree(self.TEST_BACKUP_TARFILE_FOLDER)
    #endIf

    if (os.path.exists(self.TEST_RESTORE_TARFILE_FOLDER)):
      shutil.rmtree(self.TEST_RESTORE_TARFILE_FOLDER)
    #endIf

    logging.shutdown()
  #endDef 

  def test_rsync_backup(self):
    self.assertEqual(usave.do_rsync(self.TEST_BACKUP_RSYNC_FOLDER), 0)
  #endDef

  def test_tarfile_backup(self):
    self.assertEqual(usave.make_tar(self.TEST_BACKUP_TARFILE_FOLDER), 0)
  #endDef

  def test_rsync_restore(self):
    usave.RESTORE_HOME = self.TEST_RESTORE_RSYNC_FOLDER

    self.assertEqual(usave.restore(self.TEST_BACKUP_RSYNC_FOLDER), 0)

    compare = ['diff', '-r', '-q', self.TEST_BACKUP_RSYNC_FOLDER, self.TEST_RESTORE_RSYNC_FOLDER]
    self.assertEqual(usave._execute(compare), 0)
  #endDef

  def test_tarfile_restore(self):
    usave.RESTORE_HOME = self.TEST_RESTORE_TARFILE_FOLDER
    tarfile = os.path.join(self.TEST_BACKUP_TARFILE_FOLDER, os.path.split(self.TEST_BACKUP_TARFILE_FOLDER)[1] + ".tar.gz")

    self.assertEqual(usave.restore(self.TEST_BACKUP_TARFILE_FOLDER), 0)

    compare = ['tar', '-df', tarfile, '-C', self.TEST_RESTORE_TARFILE_FOLDER]
    self.assertEqual(usave._execute(compare), 0)
  #endDef
#endClass

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Main
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if __name__ == '__main__':
  unittest.main()
#endIf