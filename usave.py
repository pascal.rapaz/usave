#!/usr/bin/env python
# -*- coding: utf-8 -*-

##
# =============================================================================
#
# This scirpt help you to do a partial or full backup of your installation
#
# Author : Pascal Rapaz
# Date   : 11.10.2014 Creation
# Version: 1.0.0
#
# References:
#   https://wiki.archlinux.org/index.php/Full_system_backup_with_rsync
#   http://www.aboutdebian.com/tar-backup.htm
#
# License: GPL-2+
#   This package is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This package is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   If you need the GNU General Public License write to:
#     Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
#     MA 02110-1301, USA.
#
# (c) 2014, Pascal Rapaz (pascal.rapaz@rapazp.ch)
# =============================================================================
##

# __builtin__ is used to define inter-module variable "__dryrun__"
import __builtin__

import argparse
import os
import os.path as path
import logging
import subprocess
import sys
import textwrap
import time

VERSION = "1.0.0"
VERSION_MSG = "Application     : %s" % __file__ +\
              "\nRelease version : %s" % VERSION +\
              "\nCopyright (C) 2014 Pascal Rapaz"

LOGGER_NAME = "log"

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Configuration
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

PARTIAL_BACKUP = ["/home", "/root", "/opt", "/etc/apt/sources.list.d", 
                  "/etc/apt/trustdb.gpg", "/etc/apt/trusted.gpg"]

FULL_EXCLUDE = ["/dev", "/proc", "/sys", "/tmp", "/run", "/mnt", "/media",
                "/lost+found"]

RESTORE_HOME = "/"

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Public methods
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def do_rsync(dest, full_backup=False):
  '''
  Launch rsync to backup your drive

  @param dest: destination folder
  @full_backup: if True do a full backup else do a partial backup based on
                folder list defined in the PARTIAL_BACKUP constant
  '''

  # prepare command
  command = ['rsync', '-aAXvR', '--delete', '--progress']

  if __dryrun__:
    command.append('-n')
  elif not os.path.exists(dest):
    os.makedirs(dest)
  #endIf

  if full_backup:
    command.append('/')

    if 0 != len(FULL_EXCLUDE):
      for i in FULL_EXCLUDE:
        command.append('--exclude')
        command.append(i)
      #endFor
    #endIf

  else:
    for i in PARTIAL_BACKUP:
      command.append(i[:-1] if i.endswith('/') else i)
    #endFor
  #endIf

  command.append(dest)

  # dry_run is managed with the rsync '-n' options
  return _execute(command, False)
#endDef

def make_tar(dest, full_backup=False):
  '''
  Create a tag.gz file with all backuped files. The file name is saved inside
  the destination folder with the same as the folder. Once the tar.gz file is
  created, the destination folder is cleaned.

  I don't use python tarfile to create the archive because I need to keep
  permission and owner. Doing it with standard command seens to be more simple.

  @param dest: destination folder
  @full_backup: if True do a full backup else do a partial backup based on
                folder list defined in the PARTIAL_BACKUP constant
  '''

  tarfile = path.join(dest, path.split(dest)[1] + ".tar.gz")

  # prepare command
  command = ['tar', '-cvpzf', tarfile]

  if not os.path.exists(dest):
    os.makedirs(dest)
  #endIf

  if full_backup:
    command.append('/')

    if 0 != len(FULL_EXCLUDE):
      for i in FULL_EXCLUDE:
        command.append('--exclude')
        command.append(i)
      #endFor
    #endIf

  else:
    for i in PARTIAL_BACKUP:
      command.append(i[:-1] if i.endswith('/') else i)
    #endFor
  #endIf

  return _execute(command, __dryrun__)
#endDef

def restore(source):
  '''
  Restore the backup into root folder (/).
  This funiction check if there is a tarfile inside folder else use rsync to do
  the restore

  @param source: Folder used to do the backup.
  '''

  source = source[:-1] if source.endswith('/') else source
  tarfile = path.join(source, path.split(source)[1] + ".tar.gz")

  if not os.path.exists(source):
    log.error("'Backup folder not found")
    sys.exit(1)
  #endIf

  if not os.path.exists(RESTORE_HOME):
    os.makedirs(RESTORE_HOME)
  #endIf

  command = []
  if os.path.exists(tarfile):
    command.extend(['tar', 'xvfz', tarfile, '-C', RESTORE_HOME])
  else:
    source += '/'

    command.extend(['rsync', '-aAXv', '--progress', source, RESTORE_HOME])
  #endIf

  return _execute(command, __dryrun__)
#endDef

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Private methods
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _check_rights():

  # check for sudo
  euid = os.geteuid()
  if euid != 0:
    log.warning("Script not started as root. Running sudo...")
    args = ['sudo', sys.executable] + sys.argv + [os.environ]
    # the next line replaces the currently-running process with the sudo
    os.execlpe('sudo', *args)
  #endIf
#endDef

def _execute(command, dry_run=False):
  '''
  Execute a command in a shell, wait for termination then return the execution
  code

  @param command: should be a sequence of program arguments or else a single
                  string
  '''

  log = logging.getLogger(LOGGER_NAME)
  log.info(command)

  if not dry_run:
    p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    while p.poll() is None:
      out = p.stdout.readline()

      sys.stdout.write(out)
      sys.stdout.flush()
    #endWhile

    return p.returncode
  else:
    return 0
  #endIf
#endDef

def _logger(name, log_file=None, level=logging.INFO):
  '''
  Create a new logger

  @param name: Name of the logger
  @param log_file: Create a log file if provided
  @param level: Log level according to the logging class (Default: logging.INFO)

  @return the logger
  '''

  # create logger
  log = logging.getLogger(name)
  log.setLevel(level)

  # create formatter
  formatter = logging.Formatter("[%(levelname)s] %(message)s")

  # create console handler
  strm_hdlr = logging.StreamHandler()
  strm_hdlr.setLevel(level)
  strm_hdlr.setFormatter(formatter)
  log.addHandler(strm_hdlr)

  if log_file:
    file_hdlr = logging.FileHandler(log_file, mode='w')
    file_hdlr.setLevel(level)
    file_hdlr.setFormatter(formatter)
    log.addHandler(file_hdlr)
  #endIf

  return log
#endDef

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Startup methods
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_arguments():
  '''
  Retrieve command line arguments
  '''

  parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                   version=VERSION_MSG,
                                   description=textwrap.dedent("""
        Small tool designed to help backup a GNU/Linux installation.
        This tool use rsync to backup your files or a tarfile to create a
        compressed backup"""))

  parser.add_argument('store_folder',
                      help='Folder used to store a backup')

  parser.add_argument("-c", "--compress",
                      action="store_true",
                      help="Create a compressed backup file using a tarfile (tar.gz)")

  parser.add_argument("-f", "--full-backup",
                      dest="fullbackup",
                      action="store_true",
                      help="Perform a backup from root folder (/) instead of folders specified in the PARTIAL_BACKUP list constant")

  parser.add_argument("-r", "--restore",
                      action="store_true",
                      help="Perform a restore from a backup folder")

  parser.add_argument("-n", "--dry-run",
                      dest="dryrun",
                      action="store_true",
                      help="Perform a trial run with no changes made")

  return parser.parse_args()
#endDef

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Launch script
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if __name__ == "__main__":

  args = get_arguments()

  __builtin__.__dryrun__ = (args.dryrun) and True or False

  log = _logger(LOGGER_NAME)

  _check_rights()

  start = time.time()

  try:
    if not args.restore:
      if args.compress:
        make_tar(args.store_folder, args.fullbackup)
      else:
        do_rsync(args.store_folder, args.fullbackup)
      #endIf
    else:
      restore(args.store_folder)
    #endIf
  finally:
    formatted_time = time.strftime("%H:%M:%S", time.gmtime(time.time() - start))

    msg = "Execution time %s" % (formatted_time)
  #endTry

  if __dryrun__:
    log.warn('Execution in DRY-RUN mode')
  #endIf

  log.info(msg)
  logging.shutdown()
#endIf
