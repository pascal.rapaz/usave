usave.py
========

`usave` is a small tool for GNU/Linux designed to simplify a partial or full backup of your installation. It use `rsync` to backup your files or a `tarfile` to do a backup in a single compressed file.

Partial backup is usefull if you plan to do a fresh GNU/Linux installation. It will save your personnal datas and some configuration files.

Moreover, any backup done in `rsync` mode can be updated if you are using the same folder than before. But be aware that extra files in the destination will be deleted since the option `--delete` is used with `rsync`. 
However, this rule doesn't apply when you restore from a `rsync` backup, in this case, the option `--delete` is not set, so extra files are maintained.

##Usage

###Partial backup
By default, a partial backup will save a folder list defined with the constant `PARTIAL_BACKUP` inside `usave.py`. This list can be edited but you must keep in mind that all path must be absolute from root folder ('/').

```
$ ./usave.py <destination>      # rsync backup
or
$ python usave.py <destination> # rsync backup


$ ./usave.py -c <destination>      # tarfile backup
or
$ python usave.py -c <destination> # tarfile backup
```

###Full backup
Create a full backup of your system. A list of folder to exclude can be defined in the constant `FULL_EXCLUDE` inside `usave.py`. Like for a partial backup, all path must be absolute from root ('/')

```
$ ./usave.py -f <destination>      # rsync backup
or
$ python usave.py -f <destination> # rsync backup


$ ./usave.py -f -c <destination>      # tarfile backup
or
$ python usave.py -f -c <destination> # tarfile backup
```

###Restore
Restore the backup in the root folder ('/') by default. You can change the destination folder by editing the constant `RESTORE_HOME` inside `usave.py`.

Note: The choice between rsync or tarfile to restore a backup is done by the application

```
$ ./usave.py -r <backup_folder>      # restore the backup
or
$ python usave.py -r <backup_folder> # restore the backup
```

##References
- https://wiki.archlinux.org/index.php/Full_system_backup_with_rsync
- http://www.aboutdebian.com/tar-backup.htm
